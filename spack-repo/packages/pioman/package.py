from spack.package import *

import platform
import os

from spack.pkg.pm2.puk import Puk

class Pioman(AutotoolsPackage):
    """PIOMan I/O manager.

    PIOMan is a generic I/O manager, designed to deal with
    interactions between communication and multi-threading. It
    guarantees a good level of reactivity, is able to automatically
    choose between active polling and blocking calls depending on the
    context, and may offload I/O requests to idle cores when availble
    to handle multiple requests in parallel and overlap communication
    and computation.
    """
    homepage = "https://pm2.gitlabpages.inria.fr/pioman/"
    url      = "https://pm2.gitlabpages.inria.fr/releases/pm2-2024-11-21.tar.gz"
    list_url = "https://pm2.gitlabpages.inria.fr/releases/"
    git      = 'https://gitlab.inria.fr/pm2/pm2.git'

    maintainers('a-denis')
    license('GPL-2.0-or-later', checked_by='a-denis')

    def url_for_version(self, version):
        url = "https://pm2.gitlabpages.inria.fr/releases/pm2-{0}.tar.gz"
        return url.format(version)

    version('master', branch='master')
    version("2024-11-21", sha256="76da169bbb9720a13be1f750480e1a7d6510830163878852876932639879d632")
    version('2024-07-12', sha256="ea9bb91b213950a52eb99d787110905d45ed02954ea9133596d690db5be0c31b")
    version('2022-05-31', sha256="afd19809a5a520a477ab596f951bbde3209868ab16febbc246592e8aed20c3ca")
    version('2021-05-21', sha256="6a207b032e623b8be0196a42dcaf4311bfe45ede2e044bd47611b6610c04c61e")

    variant('optimize', default=True,  description='Build in optimized mode')
    variant('debug',    default=False, description='Build in debug mode')
    variant('asan',     default=False, description='Build with Address Sanitizer (ASAN)')
    variant('pthread',  default=True,  description='use pthreads')

    depends_on('c',         type='build')
    depends_on('pkgconfig', type='build')
    depends_on('autoconf@2.69:', type='build')
    depends_on('gmake',     type='build')

    depends_on('hwloc', type=('build', 'link', 'run'))

    for v in Puk.versions:
        depends_on(f"puk@{v}", when=f"@{v}")

    depends_on('puk')
    depends_on('puk+asan', when='+asan')

    conflicts("platform=darwin", msg="Darwin is not supported.")
    conflicts("platform=windows", msg="Windows is not supported.")
    conflicts("%gcc@:5", msg="Requires at least gcc 6.")


    configure_directory = 'pioman'
    build_directory = 'build'

    def autoreconf(self, spec, prefix):
        w = os.getcwd()
        os.chdir(self.configure_directory)
        autogen = Executable("./autogen.sh")
        autogen()
        os.chdir(w)

    def configure_args(self):
        spec = self.spec
        config_args = [
            '--enable-optimize' if '+optimize' in spec else '--disable-optimize',
            '--enable-debug'    if '+debug'    in spec else '--disable-debug',
            '--enable-asan'     if '+asan'     in spec else '--disable-asan',
            '--with-pthread'    if '+pthread'  in spec else '--without-pthread',
            '--with-hwloc',      # always use hwloc
            '--without-gtg',     # no gtg package in spack
            '--without-simgrid', # no simgrid support in spack for now
            ]
        return config_args
