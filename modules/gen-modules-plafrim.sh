#! /bin/bash

module purge

module load tools/module_cat

module load hardware/hwloc
module load compiler/gcc
module load tools/git
module load build/autotools

# generate plafrim modules for the latest git version

PREFIX_ROOT=/cm/shared/dev/modules/generic/apps/mpi/
MODULE_ROOT=/cm/shared/dev/modules/generic/modulefiles/mpi/
#PREFIX_ROOT=$PWD/test-prefix
#MODULE_ROOT=$PWD/test-module

# clone repo to get version

GIT="git@gitlab.inria.fr:pm2/pm2.git"
echo
echo "# cloning ${GIT}..."
echo
git clone ${GIT}

version=$( ./pm2/building-tools/package-version.sh ./pm2/building-tools ./pm2 )

rm -rf ./pm2

echo "# version = ${version}"
if [ x${version} = x ]; then
    exit 1
fi
echo "ok?"
read

for flavor in madmpi madmpi-nothread; do

    echo
    echo "# starting flavor ${flavor}"
    echo "#   prefix = ${PREFIX_ROOT}/${flavor}/${version}/"
    echo "#   module = ${MODULE_ROOT}/${flavor}/${version}"

    ./gen-module.sh ${PREFIX_ROOT}/${flavor}/${version} ${MODULE_ROOT}/${flavor}/${version} ${flavor}.conf
    rc=$?

    if [ ${rc} != 0 ]; then
        exit 1
    fi
    
    cat >> ${MODULE_ROOT}/${flavor}/${version} << EOF

setenv           PIOM_ENABLE_PROGRESSION 0
conflict         mpi/openmpi
conflict         mpi/madmpi
conflict         mpi/madmpi-nothread
prereq           hardware/hwloc

EOF

    module_perm -f ${PREFIX_ROOT}/${flavor}/${version}
    module_perm -f ${MODULE_ROOT}/${flavor}/${version}
    
done

