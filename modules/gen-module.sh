#! /bin/bash

# NewMadeleine
# Copyright (C) 2024 (see AUTHORS file)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

GIT="git@gitlab.inria.fr:pm2/pm2.git"

prefix=$1
module=$2
flavor=$3

usage() {
    echo "usage: gen-module.sh <prefix> <module> <flavor>"
}

if [ x${prefix} = x ]; then
    echo " ** missing prefix"
    usage
    exit 1
fi
if [ x${module} = x ]; then
    echo " ** missing module path"
    usage
    exit 1
fi
if [ x${flavor} = x ]; then
    echo " ** missing flavor"
    usage
    exit 1
fi

echo
echo "# checking environment..."
echo

if pkg-config --exists Puk; then
    echo " ** Puk detected in environment. We need a clean environment to build the module."
    exit 1
fi
if pkg-config --exists PukABI; then
    echo " ** PukABI detected in environment. We need a clean environment to build the module."
    exit 1
fi
if pkg-config --exists PadicoTM; then
    echo " ** PadicoTMk detected in environment. We need a clean environment to build the module."
    exit 1
fi
if pkg-config --exists nmad; then
    echo " ** nmad detected in environment. We need a clean environment to build the module."
    exit 1
fi
if pkg-config --exists pioman; then
    echo " ** pioman detected in environment. We need a clean environment to build the module."
    exit 1
fi


if [ -r ./pm2 ]; then
    echo
    echo "# purging previous srcdir"
    echo
    rm -rf ./pm2
fi


echo
echo "# cloning ${GIT}..."
echo
git clone ${GIT}

echo
echo "# building..."
echo

./pm2/scripts/pm2-build-packages --prefix=${prefix} --purge --builddir=./build ${flavor}
rc=$?
if [ ${rc} != 0 ]; then
    exit 1
fi

echo
echo "# generating module ${module}..."
echo

d=$( dirname ${module} )
if [ x${d} != x ]; then
    mkdir -p ${d}
fi

cat > ${module} <<EOF
#%Module

proc ModulesHelp { } {

        puts stderr "\tAdd MadMPI library to your environment (https://pm2.gitlabpages.inria.fr/newmadeleine/)"
}

module-whatis    "Add MadMPI library to your environment (https://pm2.gitlabpages.inria.fr/newmadeleine/)"

set              prefix           ${prefix}

prepend-path     PATH             \$prefix/bin
prepend-path     LD_LIBRARY_PATH  \$prefix/lib
prepend-path     PKG_CONFIG_PATH  \$prefix/lib/pkgconfig
prepend-path     CPATH            \$prefix/include
prepend-path     LIBRARY_PATH     \$prefix/lib

setenv           MPI_HOME         ${prefix}
setenv           MPI_RUN          ${prefix}/bin/mpirun
setenv           MPI_NAME         madmpi
setenv           MPI_VER          4.0
EOF

echo "# --------------------------------"
cat ${module}
echo "# --------------------------------"

echo
echo "# cleaning srcdir & builddir"
echo
rm -rf ./pm2 ./build

echo
echo "# done."

